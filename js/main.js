var DSSV = [];

var dataJson = localStorage.getItem("DSSV_LOCAL");
if (dataJson != null) {
    var dataArr = JSON.parse(dataJson);
    DSSV = dataArr.map(function(item) {
        var sv = new sinhVien(
            item.maSV, 
            item.tenSV, 
            item.emailSV, 
            item.matKhauSV, 
            item.diemToan,
            item.diemLy,
            item.diemHoa)
        return sv;
    })
    renderDSSV(DSSV);
}

// thêm sv
function themSV() {
    var sv = layThongTinTuForm();
    DSSV.push(sv);
    var dssvJson = JSON.stringify(DSSV);
    localStorage.setItem("DSSV_LOCAL", dssvJson);
    renderDSSV(DSSV);
}

// xóa sv
function xoaSV(idSV) {
    var viTri = timKiemViTri(idSV, DSSV);
    if (viTri != -1) {
        DSSV.splice(viTri, 1);
        renderDSSV(DSSV);
    }
}

// sửa sv
function suaSV(idSV) {
    var viTri = timKiemViTri(idSV, DSSV);
    if (viTri != -1) {
        var sv = DSSV[viTri];
        document.getElementById("txtMaSV").disabled = true;
    }
}