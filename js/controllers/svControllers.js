function layThongTinTuForm() {
    var _maSV = document.getElementById("txtMaSV").value;
    var _tenSV = document.getElementById("txtTenSV").value;
    var _emailSV = document.getElementById("txtEmail").value;
    var _matKhauSV = document.getElementById("txtPass").value;
    var _diemToan = document.getElementById("txtDiemToan").value * 1;
    var _diemLy = document.getElementById("txtDiemLy").value * 1;
    var _diemHoa = document.getElementById("txtDiemHoa").value * 1;

    return new sinhVien(_maSV, _tenSV, _emailSV, _matKhauSV, _diemToan, _diemLy, _diemHoa);
}

function renderDSSV(svArr) {
    var contentHTML = "";
    for (var i = 0; i < svArr.length; i++) {
        var sv = svArr[i];
        var contentTr = `   <tr>
                                <td>${sv.maSV}</td>
                                <td>${sv.tenSV}</td>
                                <td>${sv.emailSV}</td>
                                <td>${sv.tinhDTB()}</td>
                                <td>
                                    <button onclick="xoaSV('${sv.maSV}')" class="btn btn-danger mx-1">Xóa</button>
                                    <button onclick="suaSV('${sv.maSV}')" class="btn btn-warning mx-1">Sửa</button>
                                </td>
                            </tr>`
        contentHTML += contentTr;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
    var viTri = -1;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].maSV == id) {
            viTri = i;
        }
    }
    return viTri;
}